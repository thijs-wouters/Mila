defprotocol Mila.HTML do
  def convert(source)
end

defimpl Mila.HTML, for: BitString do
  def convert(source) when is_binary(source), do: source
end

defimpl Mila.HTML, for: List do
  def convert(source), do: source
end
