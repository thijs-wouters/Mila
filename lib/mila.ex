defmodule Mila do
  defmacro __using__(_opts) do
    quote do
      import Mila
    end
  end

  def text(html) do
    html
    |> Mila.HTML.convert
    |> Floki.text
  end

  def text(html, selector) do
    html
    |> Mila.HTML.convert
    |> Floki.find(selector)
    |> Floki.text
  end

  def attribute(html, attribute) do
    html
    |> Mila.HTML.convert
    |> Floki.attribute(attribute)
    |> Enum.join
  end

  def attribute(html, selector, attribute) do
    html
    |> Mila.HTML.convert
    |> Floki.attribute(selector, attribute)
    |> Enum.join
  end

  def count(html, selector) do
    html
    |> Mila.HTML.convert
    |> Floki.find(selector)
    |> Enum.count
  end

  def present?(html, selector) do
    count(html, selector) > 0
  end

  def inside(html, selector, func) do
    html
    |> Mila.HTML.convert
    |> Floki.find(selector)
    |> func.()
  end
end
