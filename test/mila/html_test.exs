defmodule Mila.HTMLTest do
  use ExUnit.Case

  test "can convert a String" do
    assert Mila.HTML.convert("<html></html>") == "<html></html>"
  end
end
