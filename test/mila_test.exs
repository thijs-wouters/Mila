defmodule MilaTest do
  use ExUnit.Case
  use Mila

  doctest Mila

  @html """
  <html>
    <body>
      <p class="test">Expected</p>
      <ul>
        <li>One</li>
        <li>Two</li>
        <li>Three</li>
      </ul>
      <div class="limiting">
        <div class="limit">Expected</div>
        <div class="nesting">
          <div class="nested">Nested</div>
        </div>
      </div>
      <div class="limit">Not expected</div>
    </body>
  </html>
  """

  test "text retrieves the text of an element" do
    assert text(@html, ".test") == "Expected"
  end

  test "text can retrieve the text of the current element" do
    assert text("<div>Expected</div>") == "Expected"
  end

  test "attributes retrieves the value of a given attribute" do
    assert attribute(@html, ".test", "class") == "test"
  end

  test "attributes can retrieve an attribute of the current element" do
    assert attribute("<div class=\"test\"></div>", "class") == "test"
  end

  test "count counts the number of occurences of a tag" do
    assert count(@html, "li") == 3
  end

  test "inside limits search to subset" do
    inside(@html, ".limiting", fn(inside) ->
      assert count(inside, ".limit") == 1
      assert text(inside, ".limit") == "Expected"
      assert attribute(inside, ".limit", "class") == "limit"
    end)
  end

  test "you can nest inside calls" do
    inside(@html, ".limiting", fn(inside) ->
      inside(inside, ".nesting", fn(nested) ->
        assert count(nested, ".nested") == 1
        assert text(nested, ".nested") == "Nested"
        assert attribute(nested, ".nested", "class") == "nested"
      end)
    end)
  end

  test "can find if a selector is present" do
    assert present?(@html, ".test")
    refute present?(@html, ".not-present")
  end
end
